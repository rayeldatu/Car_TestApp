package com.test.rayel.testapp.presenter.car_list;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.test.rayel.testapp.R;
import com.test.rayel.testapp.model.api.ApiUtils;
import com.test.rayel.testapp.model.pojo.ResponsePojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rayel on 1/27/2018.
 */

public class CarListManagerImpl implements CarListManager {
    Context mContext;
    CarListManager.CarListManagerListener mCarListManagerListener;

    public CarListManagerImpl(Context context) {
        mContext = context;
    }

    @Override
    public void getCarList(int page, int maxItems, String sortOrder) {

        ApiUtils.getCarListApi(mContext).carList(page, maxItems, sortOrder).enqueue(new Callback<ResponsePojo>() {
            @Override
            public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                if (response.isSuccessful()) {
                    mCarListManagerListener.onCarListUpdate(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsePojo> call, Throwable t) {
                Log.e("API", t.getMessage());
                mCarListManagerListener.onFailure();
            }
        });
    }

    @Override
    public void getCarListWithNewSorting(String sortType) {

    }

    @Override
    public void registerListener(CarListManagerListener carListManagerListener) {
        mCarListManagerListener = carListManagerListener;
    }

    @Override
    public void loadMore(int nextPage, String sortOrder) {
        ApiUtils.getCarListApi(mContext).carList(nextPage, mContext.getResources().getInteger(R.integer.default_max_items), sortOrder).enqueue(new Callback<ResponsePojo>() {
            @Override
            public void onResponse(Call<ResponsePojo> call, Response<ResponsePojo> response) {
                if (response.isSuccessful()) {
                    mCarListManagerListener.onLoadMore(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsePojo> call, Throwable t) {
                Log.e("API", t.getMessage());
                mCarListManagerListener.onFailure();
            }
        });
    }

}
