package com.test.rayel.testapp.model.api;

import android.content.Context;

/**
 * Created by Rayel on 1/28/2018.
 */

public class ApiUtils {
    public static final String BASE_URL = "https://www.carmudi.co.id/";

    public static CarListApi getCarListApi(Context context) {
        return RetrofitClient.getClient(BASE_URL,context).create(CarListApi.class);
    }
}
