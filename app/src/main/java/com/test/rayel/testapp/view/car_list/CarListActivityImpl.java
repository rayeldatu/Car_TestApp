package com.test.rayel.testapp.view.car_list;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.rayel.testapp.R;
import com.test.rayel.testapp.adapters.CarListAdapter;
import com.test.rayel.testapp.model.pojo.ResponsePojo;
import com.test.rayel.testapp.presenter.car_list.CarListManager;
import com.test.rayel.testapp.presenter.car_list.CarListManagerImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rayel on 1/27/2018.
 */

public class CarListActivityImpl implements CarListManager.CarListManagerListener {
    private View mRootView;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private CarListAdapter mAdapter;
    private CarListManagerImpl mCarListManagerImpl;
    private final String TAG = "CarListActivityImpl";
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private long mProductCount = 0;
    private boolean mLoading;
    private int mCurrentPage = 1;
    private String mCurrentSortOrder;

    public CarListActivityImpl(Context context, ViewGroup container) {
        mContext = context;
        AppCompatActivity appCompatActivity = (AppCompatActivity) context;

        mRootView = LayoutInflater.from(context).inflate(R.layout.product_list_layout, container);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mRootView.findViewById(R.id.swipe_refresh_container);
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.product_list);

        mAdapter = new CarListAdapter(context, new ArrayList<ResponsePojo.Response.Listings>(), new CarListAdapter.PostItemListener() {
            @Override
            public void onPostClick(ResponsePojo.Response.Listings item) {
                String[] imageURLs = new String[item.getImages().size()];
                for (int index = 0; index < item.getImages().size(); index++) {
                    imageURLs[index] = item.getImages().get(index).getUrl();
                }
                CarDetailsDialog carDetailsDialog = CarDetailsDialog.newInstance(item.getData().getName(), item.getData().getPrice(), item.getData().getBrand(), item.getData().getBrand_model(), item.getData().getDescription(), imageURLs);
                carDetailsDialog.show(((AppCompatActivity) context).getFragmentManager(), TAG);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);

        mCarListManagerImpl = new CarListManagerImpl(context);
        mCarListManagerImpl.registerListener(this);

        String defaultSortOrder = mContext.getResources().getString(R.string.sort_newest);
        refreshCarList(defaultSortOrder);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int firstVisisbleItem, visibleItemCount, totalItemCount;
            private int visibleThresold = 10;
            private int prevTotal = 0;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisisbleItem = layoutManager.findFirstVisibleItemPosition();

                if (mLoading) {
                    if (totalItemCount > prevTotal) {
                        mLoading = false;
                        prevTotal = totalItemCount;
                    }

                } else if (!mLoading && (totalItemCount - visibleItemCount) <= (firstVisisbleItem + visibleThresold)) {
                    Log.d("...", "loading");
                    mLoading = true;
                    mCarListManagerImpl.loadMore(mCurrentPage++, mCurrentSortOrder);
                }
            }

        });
    }

    public void refreshCarList(String sortOrder) {
        mAdapter.clearItems();
        mSwipeRefreshLayout.setRefreshing(true);
        int defaultPage = mContext.getResources().getInteger(R.integer.default_page);
        int defaultMaxItems = mContext.getResources().getInteger(R.integer.default_max_items);
        mCarListManagerImpl.getCarList(defaultPage, defaultMaxItems, sortOrder);
    }

    public View getRootView() {
        return mRootView;
    }

    public void updateAdapter(List<ResponsePojo.Response.Listings> results) {
        mAdapter.updateProductList(results);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCarListUpdate(ResponsePojo carProductList) {
        hideLoading();
        mLoading = false;
        mCurrentPage = 1;
        mProductCount = carProductList.getMetadata().getProduct_count();
        updateAdapter(carProductList.getMetadata().getResults());
    }

    @Override
    public void onLoadMore(ResponsePojo carProductList) {
        mLoading = false;
        mAdapter.addItems(carProductList.getMetadata().getResults());
    }

    @Override
    public void onFailure() {
        hideLoading();
    }

    public void sortWithNewSortOrder(String sortOrder) {
        mCurrentSortOrder = sortOrder;
        refreshCarList(sortOrder);
    }

    private void hideLoading() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}
