package com.test.rayel.testapp.view.car_list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.test.rayel.testapp.R;
import com.test.rayel.testapp.presenter.car_list.CarListInteraction;

/**
 * Created by Rayel on 1/28/2018.
 */

public class CarListActivity extends AppCompatActivity implements CarListInteraction {
    private CarListActivityImpl mCarListActivityImpl;
    private Toolbar mToolbar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Spinner mSortOptionsSpinner;
    private String[] mSortOptions;

    @Override
    public void onItemSelected(int index) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCarListActivityImpl = new CarListActivityImpl(this, null);
        setContentView(mCarListActivityImpl.getRootView());
        mToolbar = mCarListActivityImpl.getRootView().findViewById(R.id.app_bar);
        mToolbar.setTitle("Car Listings");
        mSwipeRefreshLayout = (SwipeRefreshLayout) mCarListActivityImpl.getRootView().findViewById(R.id.swipe_refresh_container);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCarListActivityImpl.sortWithNewSortOrder(mSortOptions[mSortOptionsSpinner.getSelectedItemPosition()]);
            }
        });

        mSortOptions = getResources().getStringArray(R.array.sort_options);
        setSupportActionBar(mToolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.app_bar, menu);

        MenuItem sortOptionsMenuItem = menu.findItem(R.id.app_bar_sort);
        mSortOptionsSpinner = (Spinner) MenuItemCompat.getActionView(sortOptionsMenuItem);

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this, R.array.sort_options,/*android.R.layout.simple_spinner_dropdown_item*/ R.layout.spinner_dropdown_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSortOptionsSpinner.setAdapter(arrayAdapter);
        mSortOptionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCarListActivityImpl.sortWithNewSortOrder(mSortOptions[position].toLowerCase());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return true;
    }
}
