package com.test.rayel.testapp.presenter.car_list;

import com.test.rayel.testapp.model.pojo.ResponsePojo;

import java.util.List;

/**
 * Created by Rayel on 1/27/2018.
 */

public interface CarListManager {
    interface CarListManagerListener {
        void onCarListUpdate(ResponsePojo carProductList);

        void onLoadMore(ResponsePojo carProductList);

        void onFailure();
    }

    void getCarList(int page, int maxItems, String sortOrder);

    void getCarListWithNewSorting(String sortType);

    void registerListener(CarListManagerListener carListManagerListener);

    void loadMore(int nextPage, String sortOrder);

}
