package com.test.rayel.testapp.presenter.car_list;

/**
 * Created by Rayel on 1/27/2018.
 */

public interface CarListInteraction {
    void onItemSelected(int index);
}
