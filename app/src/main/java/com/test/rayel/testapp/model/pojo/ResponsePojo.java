package com.test.rayel.testapp.model.pojo;

import java.util.List;

/**
 * Created by Rayel on 1/29/2018.
 */

public class ResponsePojo {
    private Response metadata;

    public Response getMetadata() {
        return metadata;
    }

    public void setMetadata(Response metadata) {
        this.metadata = metadata;
    }

    public class Response {
        private List<Listings> results;
        long product_count;

        public List<Listings> getResults() {
            return results;
        }

        public void setResults(List<Listings> results) {
            this.results = results;
        }

        public long getProduct_count() {
            return product_count;
        }

        public void setProduct_count(long product_count) {
            this.product_count = product_count;
        }

        public class Listings {
            long id;
            List<Images> images;
            Details data;

            public long getId() {
                return id;
            }

            public void setId(long id) {
                this.id = id;
            }

            public List<Images> getImages() {
                return images;
            }

            public void setImages(List<Images> images) {
                this.images = images;
            }

            public Details getData() {
                return data;
            }

            public void setData(Details data) {
                this.data = data;
            }

            public class Details {
                String name;
                String price;
                String brand;
                String description;
                String brand_model;

                public String getBrand_model() {
                    return brand_model;
                }

                public void setBrand_model(String brand_model) {
                    this.brand_model = brand_model;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getBrand() {
                    return brand;
                }

                public void setBrand(String brand) {
                    this.brand = brand;
                }
            }

            public class Images {
                String url;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }


        }
    }
}
