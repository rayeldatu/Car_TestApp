package com.test.rayel.testapp.view.car_list;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.rayel.testapp.R;
import com.test.rayel.testapp.utils.Utils;

import java.text.NumberFormat;
import java.util.Locale;

import okhttp3.internal.Util;

/**
 * Created by Rayel on 1/30/2018.
 */

public class CarDetailsDialog extends DialogFragment {
    private final static String EXTRA_PRODUCT_NAME = "productName";
    private final static String EXTRA_PRODUCT_PRICE = "productPrice";
    private final static String EXTRA_PRODUCT_BRAND = "productBrand";
    private final static String EXTRA_PRODUCT_DESCRIPTION = "productDescription";
    private final static String EXTRA_PRODUCT_IMAGES = "productImages";
    private final static String EXTRA_PRODUCT_MODEL = "productModel";

    public CarDetailsDialog() {

    }

    public static CarDetailsDialog newInstance(String productName, String productPrice, String productBrand, String productModel, String productDescription, String[] imageURLs) {
        CarDetailsDialog dialog = new CarDetailsDialog();
        Bundle args = new Bundle();
        args.putString(EXTRA_PRODUCT_NAME, productName);
        args.putString(EXTRA_PRODUCT_BRAND, productBrand);
        args.putString(EXTRA_PRODUCT_PRICE, productPrice);
        args.putString(EXTRA_PRODUCT_DESCRIPTION, productDescription);
        args.putString(EXTRA_PRODUCT_MODEL, productModel);
        args.putStringArray(EXTRA_PRODUCT_IMAGES, imageURLs);
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_details_dialog, container, false);

        TextView productNameTextView = (TextView) view.findViewById(R.id.product_name);
        TextView productBrandTextView = (TextView) view.findViewById(R.id.product_brand);
        TextView productPriceTextView = (TextView) view.findViewById(R.id.product_price);
        TextView productDescriptionTextView = (TextView) view.findViewById(R.id.product_description);
        LinearLayout imageContainer = (LinearLayout) view.findViewById(R.id.image_container);

        productNameTextView.setText(getArguments().getString(EXTRA_PRODUCT_NAME));
        productBrandTextView.setText(getArguments().getString(EXTRA_PRODUCT_BRAND) + " - " + getArguments().getString(EXTRA_PRODUCT_MODEL));
        productDescriptionTextView.setText(getArguments().getString(EXTRA_PRODUCT_DESCRIPTION));

        productDescriptionTextView.setMovementMethod(new ScrollingMovementMethod());

        String productPrice = getArguments().getString(EXTRA_PRODUCT_PRICE);
        if (productPrice != null) {
            double carPrice = Double.parseDouble(productPrice);
            productPriceTextView.setText(NumberFormat.getNumberInstance(Locale.US).format(carPrice));
        } else {
            productPriceTextView.setText(productPrice);
        }

        String[] imageURLs = getArguments().getStringArray(EXTRA_PRODUCT_IMAGES);
        for (String url : imageURLs) {
            ImageView imageView = new ImageView(getActivity());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utils.toDP(128, getActivity()), Utils.toDP(128, getActivity()));
            params.setMargins(Utils.toDP(12, getActivity()), 0, Utils.toDP(12, getActivity()), 0);
            imageView.setLayoutParams(params);
            try {
                Picasso.with(getActivity())
                        .load(url)
                        .error(android.R.drawable.ic_menu_close_clear_cancel)
                        .into(imageView);
            } catch (NullPointerException e) {
                Log.e("API", e.getMessage());
            }
            imageContainer.addView(imageView);
        }

        return view;
    }
}
