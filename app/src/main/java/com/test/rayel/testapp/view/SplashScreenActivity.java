package com.test.rayel.testapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.test.rayel.testapp.view.car_list.CarListActivity;

/**
 * Created by Rayel on 1/30/2018.
 */

public class SplashScreenActivity extends AppCompatActivity {
    //Splash Screen activity only

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(getApplicationContext(), CarListActivity.class);
        startActivity(intent);
    }
}
