package com.test.rayel.testapp.utils;

import android.content.Context;

/**
 * Created by Rayel on 1/30/2018.
 */

public class Utils {
    public static int toDP(int px, Context context) {
        return (int) (px * context.getResources().getDisplayMetrics().density);
    }
}
