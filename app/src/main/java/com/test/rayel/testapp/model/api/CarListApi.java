package com.test.rayel.testapp.model.api;

import com.test.rayel.testapp.model.pojo.ResponsePojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Rayel on 1/28/2018.
 */

public interface CarListApi {
    @GET("/api/cars/page:{page}/maxitems:{maxItems}/sort:{sortKey}")
    Call<ResponsePojo> carList(@Path("page") int page, @Path("maxItems") int maxItems, @Path("sortKey") String sortKey);
}
