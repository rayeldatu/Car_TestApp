package com.test.rayel.testapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.rayel.testapp.R;
import com.test.rayel.testapp.model.pojo.ResponsePojo;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rayel on 1/28/2018.
 */

public class CarListAdapter extends RecyclerView.Adapter<CarListAdapter.CarListViewHolder> {
    private List<ResponsePojo.Response.Listings> mCarListResult;
    private Context mContext;
    private PostItemListener mItemListener;

    public void updateProductList(List<ResponsePojo.Response.Listings> results) {
        mCarListResult = results;
        notifyDataSetChanged();
    }

    public void addItems(List<ResponsePojo.Response.Listings> results) {
        mCarListResult.addAll(results);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mCarListResult.clear();
        notifyDataSetChanged();
    }

    public class CarListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private PostItemListener mItemListener;
        public ImageView mProductImage;
        public TextView mProductName, mProductPrice, mProductBrand;

        public CarListViewHolder(View itemView, PostItemListener itemListener) {
            super(itemView);

            mProductImage = (ImageView) itemView.findViewById(R.id.product_image);
            mProductName = (TextView) itemView.findViewById(R.id.product_name);
            mProductPrice = (TextView) itemView.findViewById(R.id.product_price);
            mProductBrand = (TextView) itemView.findViewById(R.id.product_brand);

            this.mItemListener = itemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            this.mItemListener.onPostClick(mCarListResult.get(getAdapterPosition()));
            //notifyDataSetChanged();
        }
    }

    public CarListAdapter(Context context, List<ResponsePojo.Response.Listings> carProducts, PostItemListener itemListener) {
        mContext = context;
        mCarListResult = carProducts;
        mItemListener = itemListener;
    }


    @Override
    public CarListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.product_item_layout, parent, false);
        CarListViewHolder viewHolder = new CarListViewHolder(view, this.mItemListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CarListViewHolder holder, int position) {
        ResponsePojo.Response.Listings.Details carProduct = mCarListResult.get(position).getData();
        TextView productName = holder.mProductName;
        TextView productPrice = holder.mProductPrice;
        TextView productBrand = holder.mProductBrand;
        ImageView productImage = holder.mProductImage;

        productName.setText(carProduct.getName());

        if (carProduct.getPrice() != null) {
            double carPrice = Double.parseDouble(carProduct.getPrice());
            productPrice.setText(NumberFormat.getNumberInstance(Locale.US).format(carPrice));
        } else {
            productPrice.setText(carProduct.getPrice());
        }

        productBrand.setText(carProduct.getBrand());

        try {
            Picasso.with(mContext)
                    .load(mCarListResult.get(position).getImages().get(0).getUrl())
                    .error(android.R.drawable.stat_notify_error)
                    .into(productImage);
        } catch (NullPointerException e) {
            Log.e("API", e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return mCarListResult.size();
    }

    private ResponsePojo.Response.Listings.Details getItem(int adapterPosition) {
        return mCarListResult.get(adapterPosition).getData();
    }

    public interface PostItemListener {
        void onPostClick(ResponsePojo.Response.Listings item);
    }
}
